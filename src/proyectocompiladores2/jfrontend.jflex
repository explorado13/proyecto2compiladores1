package proyectocompiladores2;
import java_cup.runtime.Symbol;

%%
%state A
%cupsym Simboloj
%class jfrontend
%cup
%public
%unicode
%line
%char
%ignorecase

numero = [0-9]+
palabra = [a-zA-Záéíóú]([a-zA-Záéíóú]|[0-9]|\_)+

%%

"Columna"       {return new Symbol(Simboloj.icolumna, yychar,yyline); }

"A"		{return new Symbol(Simboloj.iA, yychar,yyline); }
"B"		{return new Symbol(Simboloj.iB, yychar,yyline); }
"C"		{return new Symbol(Simboloj.iC, yychar,yyline); }
"D"		{return new Symbol(Simboloj.iD, yychar,yyline); }
"E"		{return new Symbol(Simboloj.iE, yychar,yyline); }
"F"		{return new Symbol(Simboloj.iF, yychar,yyline); }
"G"		{return new Symbol(Simboloj.iG, yychar,yyline); }
"H"		{return new Symbol(Simboloj.iH, yychar,yyline); }
"I"		{return new Symbol(Simboloj.iI, yychar,yyline); }
"J"		{return new Symbol(Simboloj.iJ, yychar,yyline); }
"K"		{return new Symbol(Simboloj.iK, yychar,yyline); }
"L"		{return new Symbol(Simboloj.iL, yychar,yyline); }
"M"		{return new Symbol(Simboloj.iM, yychar,yyline); }
"N"		{return new Symbol(Simboloj.iN, yychar,yyline); }
"O"		{return new Symbol(Simboloj.iO, yychar,yyline); }
"P"		{return new Symbol(Simboloj.iP, yychar,yyline); }
"Q"		{return new Symbol(Simboloj.iQ, yychar,yyline); }
"R"		{return new Symbol(Simboloj.iR, yychar,yyline); }
"S"		{return new Symbol(Simboloj.iS, yychar,yyline); }
"T"		{return new Symbol(Simboloj.iT, yychar,yyline); }
"U"		{return new Symbol(Simboloj.iU, yychar,yyline); }
"V"		{return new Symbol(Simboloj.iV, yychar,yyline); }
"W"		{return new Symbol(Simboloj.iW, yychar,yyline); }
"X"		{return new Symbol(Simboloj.iX, yychar,yyline); }
"Y"		{return new Symbol(Simboloj.iY, yychar,yyline); }
"Z"		{return new Symbol(Simboloj.iZ, yychar,yyline); }
"CH"		{return new Symbol(Simboloj.iCH, yychar,yyline); }
"LL"		{return new Symbol(Simboloj.iLL, yychar,yyline); }
"RR"		{return new Symbol(Simboloj.iRR, yychar,yyline); }
"A1"		{return new Symbol(Simboloj.iA1, yychar,yyline); }

/* OPERADOR */
"["                {return new Symbol(Simboloj.icorchete, yychar,yyline); }
"]"                {return new Symbol(Simboloj.fcorchete, yychar,yyline); }
":"                {return new Symbol(Simboloj.dospuntos, yychar,yyline); }
","                {return new Symbol(Simboloj.coma, yychar,yyline); 


/* BLANCOS */
[ \t\r\f\n]+       { /* Se ignoran */}

/* Cualquier Otro */
.   { System.out.println("Error lexico: "+yytext());
     
 }