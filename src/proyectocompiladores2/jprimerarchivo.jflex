package proyectocompiladores2;
import java_cup.runtime.Symbol;

%%
%state A
%cupsym Simbolo
%class jprimerarchivo
%cup
%public
%unicode
%line
%char
%ignorecase

numero = (\+|\-)?[0-9]+
palabra = [a-zA-Záéíóú]([a-zA-Záéíóú]|[0-9]|\_)+
comilla = \"
patch = ("/"([0-9]|[a-zA-Záéíóú]|\_)+)+
fecha = \#

%%

"Path"       {return new Symbol(Simbolo.ipath, yychar,yyline); }
"Nombre"               {return new Symbol(Simbolo.inombre, yychar,yyline); } 
"Fecha"      {return new Symbol(Simbolo.ifecha, yychar,yyline); } 
"Proyecto"      {return new Symbol(Simbolo.iproyecto, yychar,yyline); } 

{numero}         {return new Symbol(Simbolo.numero, yychar,yyline,new String(yytext()));}
{palabra}        {return new Symbol(Simbolo.palabra, yychar,yyline,new String(yytext()));}
{patch}          {return new Symbol(Simbolo.patch, yychar,yyline,new String(yytext()));}
{comilla}        {return new Symbol(Simbolo.comilla, yychar,yyline,new String(yytext()));}
{fecha}          {return new Symbol(Simbolo.fecha, yychar,yyline,new String(yytext()));}


/* OPERADOR */
"["                {return new Symbol(Simbolo.icorchete, yychar,yyline); }
"]"                {return new Symbol(Simbolo.fcorchete, yychar,yyline); }
":"                {return new Symbol(Simbolo.dospuntos, yychar,yyline); }
";"                {return new Symbol(Simbolo.puntoycoma, yychar,yyline); }

/* BLANCOS */
[ \t\r\f\n]+       { /* Se ignoran */}

/* Cualquier Otro */
.   { System.out.println("Error lexico: "+yytext());
     
 }